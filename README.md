This second extension is a variation of the first extension. A content script (similiar to the first extension's)
and a background script are created. For the background script, I upload an image that appears as a browser button
("default_icon", Note to self: I had to resize the image to get it to work properly). Now, the changes from the content
script (the page) only occur if the button in the browser (external of the page) is clicked. So, the p tags only turn
orange if the browswer button is pressed. More importantly, I demonstrated access to the current tab, communicating
between the background and content script and getting a message from the background tab.

